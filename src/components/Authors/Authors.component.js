import React, { Component } from "react";
import { Subscription } from "react-apollo";
import gql from "graphql-tag";

import { AddAuthorForm, Author } from "to-do/components";

const authorAdded = gql`
  subscription {
    authorAdded {
      id
      firstName
      lastName
      books {
        id
      }
    }
  }
`;

export default class Authors extends Component {
  componentDidMount() {
    this.props.subscribeToMore({
      document: authorAdded,
      updateQuery: (prev, { subscriptionData }) => ({
        ...prev,
        getAuthors: [...prev.getAuthors, subscriptionData.data.authorAdded]
      })
    });
  }

  render() {
    const { data } = this.props;

    return (
      <div className={`app-container`} style={{ margin: `50px 0` }}>
        <h2>Authors</h2>
        <AddAuthorForm />
        {data && data.map(author => <Author key={author.id} {...author} />)}
        {/* <Subscription subscription={authorAdded}>
          {({ data, loading, error }) =>
            !loading &&
            !error &&
            data &&
            data.authorAdded && <Author {...data.authorAdded} />
          }
        </Subscription> */}
      </div>
    );
  }
}
