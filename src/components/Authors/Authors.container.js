import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Authors from "./Authors.component";

const getAuthors = gql`
  query {
    getAuthors {
      id
      firstName
      lastName
      books {
        id
      }
    }
  }
`;

export default () => (
  <Query query={getAuthors}>
    {({ loading, error, data, subscribeToMore }) =>
      loading ? (
        <h1>Loading...</h1>
      ) : error ? (
        <h1>Something went wrong</h1>
      ) : (
        <Authors
          data={data && data.getAuthors}
          subscribeToMore={subscribeToMore}
        />
      )
    }
  </Query>
);
