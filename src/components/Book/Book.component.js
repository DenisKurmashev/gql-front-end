import React, { PureComponent } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import { Button } from "to-do/components";

const getBooksQuery = gql`
  query {
    getBooks {
      id
      name
      description
      author {
        id
        firstName
        lastName
      }
    }
  }
`;

const removeBook = gql`
  mutation($bookId: ID!) {
    deleteBook(id: $bookId) {
      id
    }
  }
`;

export default class Book extends PureComponent {
  static defaultProps = {
    name: ``,
    description: ``
  };

  update = (store, res) => {
    const { getBooks } = store.readQuery({
      query: getBooksQuery
    });

    const data = {
      getBooks: getBooks.filter(book => book.id !== res.data.deleteBook.id)
    };

    store.writeQuery({ query: getBooksQuery, data });
  };

  render() {
    const { id: bookId, name, description, author } = this.props;

    return (
      <div className={`book-container`} onClick={this._onClick}>
        <div>
          <div className={`book-container__title`}>{name}</div>
          <div className={`book-container__author`}>
            Author: {author.firstName} {author.lastName}
          </div>
          <div className={`book-container__description`}>{description}</div>
        </div>
        <div>
          <Mutation mutation={removeBook}>
            {(onClick, { loading, error, data }) => (
              <Button
                title={`Remove`}
                onClick={() =>
                  onClick({
                    variables: { bookId },
                    update: this.update
                  })
                }
              />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}
