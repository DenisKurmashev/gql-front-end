import React, { PureComponent } from "react";

export default class Button extends PureComponent {
  static defaultProps = {
    onClick: () => {}
  };

  onClick = () => this.props.onClick && this.props.onClick();

  render() {
    const { title } = this.props;

    return (
      <div className={`button-container`} onClick={this.onClick}>
        {title}
      </div>
    );
  }
}
