import React, { PureComponent } from "react";

import { Book } from "to-do/components";

export default class App extends PureComponent {
  static defaultProps = {
    data: []
  };

  render() {
    const { data } = this.props;

    return (
      <div className={`app-container`}>
        <h2>Books</h2>
        {data.map(book => (
          <Book key={book.id} {...book} />
        ))}
      </div>
    );
  }
}
