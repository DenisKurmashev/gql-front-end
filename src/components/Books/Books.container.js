import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Books from "./Books.component";

const getBooks = gql`
  query {
    getBooks {
      id
      name
      description
      author {
        id
        firstName
        lastName
      }
    }
  }
`;

export default () => (
  <Query query={getBooks}>
    {({ loading, error, data }) =>
      loading ? (
        <h1>Loading...</h1>
      ) : error ? (
        <h1>Something went wrong</h1>
      ) : (
        <Books data={data && data.getBooks} />
      )
    }
  </Query>
);
