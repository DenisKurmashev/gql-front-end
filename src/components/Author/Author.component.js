import React, { PureComponent } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import { Button } from "to-do/components";

const removeAuthor = gql`
  mutation($authorId: ID!) {
    deleteAuthor(id: $authorId) {
      id
    }
  }
`;

export default class Author extends PureComponent {
  render() {
    const { id: authorId, firstName, lastName, books } = this.props;

    return (
      <div className={`book-container`} onClick={this._onClick}>
        <div>
          <div className={`book-container__title`}>
            {firstName} {lastName}
          </div>
          <div
            className={`book-container__description`}
            style={{ marginTop: 0 }}
          >
            Total books: {books.length}
          </div>
        </div>
        <div>
          <Mutation mutation={removeAuthor}>
            {(onClick, { loading, error, data }) => (
              <Button
                title={`Remove`}
                onClick={() =>
                  onClick({
                    variables: { authorId }
                  })
                }
              />
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}
