import React, { PureComponent } from "react";

import { Authors, Books } from "to-do/components";

export default class App extends PureComponent {
  render() {
    return (
      <div className={`app-container`}>
        <h1>My GQL app</h1>
        <Authors />
        {/* <Books /> */}
      </div>
    );
  }
}
