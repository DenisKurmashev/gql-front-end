import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

import AddAuthorForm from "./AddAuthorForm.component";

const createAuthor = gql`
  mutation($firstName: String!, $lastName: String!) {
    createAuthor(input: { firstName: $firstName, lastName: $lastName }) {
      id
      firstName
      lastName
    }
  }
`;

export default () => (
  <Mutation mutation={createAuthor}>
    {(onSubmit, { loading, error, data }) => (
      <AddAuthorForm onSubmit={onSubmit} />
    )}
  </Mutation>
);
