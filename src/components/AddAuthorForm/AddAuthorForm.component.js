import React, { PureComponent } from "react";

import { Button } from "to-do/components";

export default class AddAuthorForm extends PureComponent {
  state = {
    firstName: ``,
    lastName: ``
  };

  onChange = ({ target }) => this.setState({ [target.name]: target.value });

  render() {
    const { onSubmit } = this.props;

    return (
      <div className={`add-author-form`}>
        <input
          type="text"
          name="firstName"
          placeholder="First Name"
          value={this.state.firstName}
          onChange={this.onChange}
        />
        <input
          type="text"
          name="lastName"
          placeholder="Last Name"
          value={this.state.lastName}
          onChange={this.onChange}
        />
        <Button
          onClick={() => onSubmit({ variables: this.state })}
          title={`Add Author`}
        />
      </div>
    );
  }
}
