import React from "react";
import ReactDOM from "react-dom";

import { App } from "to-do/components";
import "to-do/styles/index.css";

// GQL
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-boost";
import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";
import { HttpLink } from "apollo-link-http";
import { split } from "apollo-link";
import { InMemoryCache } from "apollo-cache-inmemory";

const wsLink = new WebSocketLink({
  uri: `ws://localhost:4000/subscriptions`,
  options: {
    reconnect: true
  }
});

const httpLink = new HttpLink({ uri: `http://localhost:4000/graphql` });

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === "OperationDefinition" && operation === "subscription";
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({
  // link: httpLink,
  link,
  cache: new InMemoryCache()
});

const Root = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

// ---

ReactDOM.render(<Root />, document.getElementById("root"));
